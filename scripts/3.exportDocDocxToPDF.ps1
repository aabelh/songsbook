#Export all docx files from a directory in pdf files.
#Just need to prodvide source directory and output directory

$sourceDirectory = 'D:\SongsBook\'
$outputDirectory=$sourceDirectory+"pdf\songs\";

$stopwatch = New-Object System.Diagnostics.Stopwatch
$stopwatch.Start();
$word_app = New-Object -ComObject Word.Application

#Check if source directory exists, if not exit script
if (-not (Test-Path -LiteralPath $sourceDirectory -PathType Container)){
    "$sourceDirectory not exists, the script can not continue! Please provide a valid path"
    exit
}

#Check if output dir exists, if not, it will be created
if ( Test-Path -LiteralPath $outputDirectory -PathType Container ){
    "Yes, '$outputDirectory' dirs exits"
} else {
    "$outputDirectory not exists, it will be created"
    try {
        New-Item -Path  $outputDirectory -ItemType Directory -ErrorAction Stop | Out-Null #-Force
        "Successfully created directory '$outputDirectory'."
    }
    catch {
        Write-Error -Message "Unable to create directory '$outputDirectory'. Error was: $_" -ErrorAction Stop
    }
}

# This filter will find .doc as well as .docx documents
Get-ChildItem -Path $sourceDirectory -Filter *.doc? | ForEach-Object {

    $document = $word_app.Documents.Open($_.FullName)

    $pdf_filename = "$($outputDirectory)\$($_.BaseName).pdf"

    $document.SaveAs([ref] $pdf_filename, [ref] 17)
    Write-Host "Name: "$($_.FullName)

    $document.Close()
}

$word_app.Quit()
$stopwatch.Stop();
"Elapsed Time: '$stopwatch.Elapsed.TotalMinutes' minutes"
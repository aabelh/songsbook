﻿# Check if docs are consistent; 
# number from file name is the same with the number form song title(in document)
# example: file name is: 1. Isus iubit.docx the song title in file need to be: 1. Title song
# this will ensure that songsbook will be genreated correctly - TOC will be correct.

$selectedPath ="D:\SongsBook\docx\songs\"

$files=Get-ChildItem -Path $selectedPath -Filter "*.doc?"
$outputPath="$selectedPath\Checks_RESULTS.txt"

$word_app = New-Object -ComObject Word.Application


for($i=0; $i -lt $files.Length; $i++)
{
    $name=$files[$i].Name;
         
    $document = $word_app.Documents.Open($files[$i].FullName) 
    $firstP = $document.Paragraphs.First.Range.Text
    $noP=$firstP.Split(".")[0];
    $title=$firstP.Split(".")[1];
                
    $baseNam=$files[$i].BaseName;
    $line="$i. $name -- $noP -- $title"
    Write-Host $line
    #can add more rules, check if names are equlas
    if(-not($name.Contains($noP)) )
    {
        Write-Host "          ERRROR: "$line
        Add-Content $outputPath $line;
    }
    
    $document.Close();
    Start-Sleep -Seconds 0.1

}
Add-Content $outputPath "`r`n"
   
$word_app.Quit();
Add-Content $outputPath ($files.Length + " Files was parsed!`r`nPlease check if no errors ");
Write-Host "Done! Please check file: "$outputPath
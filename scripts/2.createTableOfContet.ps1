﻿#Create Table of content (TOC)
[ref]$SaveFormat = "microsoft.office.interop.word.WdSaveFormat" -as [type]

$tocPath = "D:\SongsBook\docx\2. Toc.docx"
$sourceDirectory = 'D:\SongsBook\docx\songs\'

$Word = New-Object -comobject word.application
$Word.Visible = $false
$Doc = $Word.Documents.Add()

#set 2 Columns layout 
$Doc.PageSetup.TextColumns.SetCount(2)
$Range = $Doc.Range()
#create a HashTable
$hash = $null
$hash = @{}

#Add into hash Song Name and number (extracted from filename)
Get-ChildItem -Path $sourceDirectory -Filter *.doc? | ForEach-Object {

    $textTofind = $_.BaseName;
    $no=$textTofind.Split(".")[0].Trim();
    $name=$textTofind.Split(".")[1].Trim();

    $hash.Add($name, $no);
    #Write-Host "Number:$no; Name:$name"

}

$Table = $Doc.Tables.Add($Range, $hash.LongLength+1, 2)

#sort hash by songTitle
$hash=$hash.GetEnumerator()|sort -Property name


#add header
$Table.Cell(1, 1).Range.Text = "Titlu"
$Table.Cell(1, 2).Range.Text = "Pag"
$Table.rows.add()

$rowIndex=2;


#iterate on hashTable, and add content into table
foreach ($v in $hash.GetEnumerator()) {
   
     #Write-Host "---Number:$key; Name:"$hash.$key
     $rowIndex

     $Table.Cell($rowIndex, 1).Range.Text = $v.Key
     $Table.Cell($rowIndex, 2).Range.Text = $v.Value
     $Table.rows.add()
     $rowIndex++
     

}

#this can be romved if is no needed
$Table.AutoFormat(0)
#save document
$doc.saveas([ref] $tocPath, [ref]$SaveFormat::wdFormatDocumentDefault)
$doc.close()
$word.quit()
Write-Host "Done! please check TOC: "$tocPath
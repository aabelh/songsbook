﻿#Merge pdfs file into single file. 
#Files name need to be in this format: 
#    1. AnyName.pdf
#    2. AnyName.pdf
#    3. AnyName.pdf
#    etc
# This will ensure that the document will be merged in specific order
# 1. AnyName.pdt then 2. AnyName.pdf then etc

#This script need library PdfSharp.dll

#Also need to define source directory (
Add-Type -Path D:\SongsBook\scripts\PdfSharp.dll                      
    
$sourceDirectory="D:\SongsBook\pdf\songs\";
$outputPdfFileName="SongsBook"
    
            
Function Merge-PDF {            
    Param($path, $filename)                        
            
    $output = New-Object PdfSharp.Pdf.PdfDocument            
    $PdfReader = [PdfSharp.Pdf.IO.PdfReader]            
    $PdfDocumentOpenMode = [PdfSharp.Pdf.IO.PdfDocumentOpenMode]                        
    $files=gci $path "*.pdf"

    for($j=1; $j -le $files.Length; $j++){ 
     #foreach($i in (gci $path *.pdf -Recurse)) {           
        Write-Host $i
        $i = gci $path "*.pdf" -Recurse | Where-Object {$_.Name.Split(".")[0].Equals($j.ToString())};
       
            $input = New-Object PdfSharp.Pdf.PdfDocument
            $input = $PdfReader::Open($i.fullname, $PdfDocumentOpenMode::Import)
            $input.Pages | %{$output.AddPage($_)}            
    }                
            
    $output.Save($filename)            
}
$stopwatch = New-Object System.Diagnostics.Stopwatch
$stopwatch.Start();



    Merge-PDF -path $sourceDirectory -filename $sourceDirectory$outputPdfFileName".pdf"

$stopwatch.Stop();
Write-Host "Elapsed Time: "$stopwatch.Elapsed.TotalMinutes" minutes"
Write-Host "Output file: "$sourceDirectory$outputPdfFileName".pdf"